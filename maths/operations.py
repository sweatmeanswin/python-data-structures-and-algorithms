"""

"""

import math


def gcd(a, b):
    while b:
        a, b = b, a % b
    return a


def get_primes(n):
    values = []
    d = 2
    while d * d <= n:
        while n % d == 0:
            values.append(d)
            n //= d
        d += 1
        d |= 1
    if n > 1:
        values.append(n)
    return values


def is_prime(value):
    if value < 2:
        return False
    for i in range(2, int(math.sqrt(value) + 1)):
        if value % i == 0:
            return False
    return True


def sieve_of_eratosthenes(max_value):
    flags = [True] * (max_value + 1)
    flags[0] = flags[1] = False
    prime = 2
    sq = int(math.sqrt(max_value))
    while prime <= sq:
        # false
        for i in range(prime * prime, max_value + 1, prime):
            flags[i] = False

        nxt = prime + 1
        while nxt < max_value and not flags[nxt]:
            nxt += 1
        prime = nxt
    return [idx for idx, flag in enumerate(flags) if flag]


def swap(a, b):
    b += a
    a = b - a
    b = b - a
    return a, b


def rotate_matrix(m):
    assert len(m) == len(min(m, key=len))
    assert len(m) == len(max(m, key=len))

    for layer in range(0, len(m) // 2):
        first = layer
        last = len(m) - 1 - layer
        for idx in range(first, last):
            offset = idx - first
            t = m[first][idx]
            m[first][idx] = m[last - offset][first]
            m[last - offset][first] = m[last][last - offset]
            m[last][last - offset] = m[idx][last]
            m[idx][last] = t


def print_matrix(m):
    for row in m:
        for col in row:
            print(col, end=' ')
        print('')


def make_change(n):
    denoms = [25, 10, 5, 1]
    cache = [[0 for _ in range(len(denoms))] for _ in range(n + 1)]

    def make_change_helper(amount, index):
        if cache[amount][index] > 0:
            return cache[amount][index]
        if index >= len(denoms) - 1:
            return 1
        denom = denoms[index]
        way_count = 0
        i = 0
        while i * denom <= amount:
            remaining = amount - i * denom
            way_count += make_change_helper(remaining, index + 1)
            i += 1
        cache[amount][index] = way_count
        return way_count

    return make_change_helper(n, 0)


def get_max_sum(array_list):
    max_sum, _sum = 0, 0
    max_sum_elements = []
    elements = []
    for i in range(len(array_list)):
        _sum += array_list[i]
        elements.append(array_list[i])
        if max_sum < _sum:
            max_sum = _sum
            max_sum_elements = elements.copy()
        elif _sum < 0:
            _sum = 0
            elements.clear()
    return max_sum, max_sum_elements


def get_pairs_with_sum_bf(array_list, requested_sum):
    """  """
    result = []
    for i in range(len(array_list)):
        for j in range(i + 1, len(array_list)):
            if array_list[i] + array_list[j] == requested_sum:
                result.append((array_list[i], array_list[j]))
    return result


def get_pairs_with_sum(array_list, requested_sum):
    """  """
    result = []
    unpaired = {}
    for el in array_list:
        complement = requested_sum - el
        if complement in unpaired:
            result.append((el, complement))
        else:
            unpaired.setdefault(el, 0)
            unpaired[el] += 1
    return result
