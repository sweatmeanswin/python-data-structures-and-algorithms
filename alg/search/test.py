"""

"""

from pprint import pprint
from random import randint
from time import perf_counter

from base_test import BaseTestInstance

from .base import BaseSearch
from . import ALL


class SearchTestInstance(BaseTestInstance):
    """

    """

    TEST_CASES = NotImplemented
    DEFAULT_ARRAY_SIZE = 1000000
    DEFAULT_PRECISION = 100

    def __init__(self, search_classes, array_size=DEFAULT_ARRAY_SIZE, precision=DEFAULT_PRECISION, verbose=False):
        """  """
        assert bool(search_classes), "Empty search classes list"
        for c in search_classes:
            if not issubclass(c, BaseSearch):
                raise TypeError(str(c.__class__))
        self._classes = list(search_classes)
        self._array_size = array_size
        self._precision = precision
        self._verbose = verbose
        self._result = {}

    def process(self):
        """ """
        for search_class in self._classes:
            self._result[search_class.__name__] = self._check_search(search_class)
        return self.result

    @property
    def result(self):
        """  """
        assert bool(self._result), "Empty result. Process didn't called"
        return self._result

    @classmethod
    def test(cls, show_console=False):
        """  """
        ch = cls(
            search_classes=ALL,
            precision=1,
            verbose=show_console,
        )
        ch.process()

    def _check_search(self, search_instance):
        """  """
        elapsed_time = []
        cycle_count = max(self._precision >> 5, 1)
        al = self._get_array_list(self._array_size)
        for _ in range(cycle_count):

            for _ in range(cycle_count):
                search_value = al[randint(0, len(al) - 1)]

                pf = perf_counter()
                idx = search_instance.search(al, search_value)
                elapsed_time.append(perf_counter() - pf)

                if idx == -1:
                    try:
                        al_idx = al.index(search_value)
                        raise Exception("{} return {}. But array[{}] is {}".format(
                            search_instance.__name__,
                            idx,
                            al_idx,
                            al[al_idx],
                        ))
                    except ValueError:
                        pass

        avg = sum(elapsed_time) / (self._precision ** 2)

        if self._verbose:
            print("-> algorithm: {}".format(search_instance.__name__))
            print("-> size: {}".format(self._array_size))
            print("-> precision: {}".format(self._precision))
            print("-> elapsed: {0:0.6f}".format(avg))
        return {
            'size': self._array_size,
            'precision': self._precision,
            'avg': avg,
            'min': min(elapsed_time),
            'max': max(elapsed_time),
        }

    @staticmethod
    def _get_array_list(n, mi=0, ma=10):
        return sorted([randint(mi, ma) for _ in range(n)])


if __name__ == '__main__':
    SearchTestInstance.test()
