"""

"""

from abc import abstractmethod


class BaseSearch:
    """

    """

    @classmethod
    @abstractmethod
    def search(cls, array_list, value):
        """  """
        raise NotImplementedError()
