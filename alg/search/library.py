"""

"""

from .base import BaseSearch


class PythonSearch(BaseSearch):
    """

    """

    @classmethod
    def search(cls, array_list, value):
        """  """
        try:
            return array_list.index(value)
        except ValueError:
            return -1
