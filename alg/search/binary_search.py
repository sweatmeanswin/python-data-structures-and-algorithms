"""

"""

from bisect import bisect_left

from .base import BaseSearch


class BinarySearch(BaseSearch):
    """

    """

    @classmethod
    def search(cls, array_list, value):
        """  """
        return cls._binary_search(array_list, value, 0, len(array_list))

    @classmethod
    def _binary_search(cls, array_list, value, lo, hi):
        """
        Args:
            array_list:
            value:
            lo:
            hi:
        """
        if lo >= hi:
            return -1

        mid = (lo + hi) // 2
        if value == array_list[mid]:
            return mid
        elif value > array_list[mid]:
            return cls._binary_search(array_list, value, mid + 1, hi)
        else:
            return cls._binary_search(array_list, value, lo, mid - 1)


class PythonBinarySearch(BaseSearch):
    """

    """

    @classmethod
    def search(cls, array_list, value):
        """  """
        idx = bisect_left(array_list, value)
        if idx != len(array_list) and array_list[idx] == value:
            return idx
        else:
            return -1
