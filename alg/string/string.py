"""
String algorithms
"""

from collections import Counter


def is_permutations(s1, s2):
    """ Check if string 's1' is permutation of string 's2' """
    if len(s1) != len(s2):
        return False
    cnt = Counter(s1)
    for ch in s2:
        if cnt.get(ch, 0) == 0:
            return False
        cnt[ch] -= 1
    return True


def is_chars_unique(check_str):
    """ Get count for every char in string 'check_str' """
    flags = [False] * 256
    for ch in str(check_str).lower():
        if flags[ord(ch)]:
            return False
        flags[ord(ch)] = True
    return True


def is_palindrome(check_str):
    """ Check if string 'check_str is palindrome """
    i = 0
    j = len(check_str) - 1
    while i < j:
        if check_str[i] != check_str[j]:
            return False
        i += 1
        j -= 1
    return True


def is_editable_strings(s1, s2):
    """ Check if one string if result of removing, inserting, editing one char """
    if abs(len(s1) - len(s2)) > 1:
        return False

    _s1 = s1 if len(s1) < len(s2) else s2
    _s2 = s2 if len(s1) < len(s2) else s1

    idx1, idx2 = 0, 0
    found_diff = False
    while idx1 < len(_s1) and idx2 < len(_s2):
        if _s1[idx1] != _s2[idx2]:
            if found_diff:
                return False
            found_diff = True
            if len(s1) == len(s2):
                idx1 += 1
        else:
            idx1 += 1
        idx2 += 1
    return True


def is_palindrome_permutation(check_str):
    """ Check if string 'check_str is palindrome """
    # Remove spaces and lower chars (optionally)
    _str = check_str.replace(' ', '').lower()
    counter = Counter(_str)
    chars_duplicate = len([c for c in counter.values() if c % 2 == 0])
    return chars_duplicate + (1 if len(_str) % 2 == 1 else 0) == len(counter)


def get_permutations_count(sub_str, check_str):
    """ Get count of substring of string 'check_str' which are permutations of string 'sub_str'  """
    count = 0
    idx = 0
    while idx < len(check_str) - len(sub_str) + 1:
        if check_str[idx] in sub_str and is_permutations(sub_str, check_str[idx:idx + len(sub_str)]):
            count += 1
        idx += 1
    return count


def get_palindrom_count_force(check_str):
    def is_palindrom_helper(lo, hi):
        while lo < hi:
            if check_str[lo] != check_str[hi]:
                return False
            lo += 1
            hi -= 1
        return True

    count = 0
    for i in range(len(check_str)):
        for j in range(i + 1, len(check_str)):
            if is_palindrom_helper(i, j):
                count += 1
    return count


def get_palindrom_count_optimize(check_str):
    def odd_count():
        count = 0
        for middle in range(len(check_str)):
            left = middle - 1
            right = middle + 1
            while left >= 0 and right < len(check_str) and check_str[left] == check_str[right]:
                count += 1
                left -= 1
                right += 1
        return count

    def even_count():
        count = 0
        for middle in range(len(check_str)):
            left = middle
            right = middle + 1
            while left >= 0 and right < len(check_str) and check_str[left] == check_str[right]:
                count += 1
                left -= 1
                right += 1
        return count

    return odd_count() + even_count()


def manacker(check_str):
    def even_count():
        left = 0
        right = -1
        res = [0] * len(check_str)
        for i in range(len(check_str)):
            temp = (0 if i > right else min(res[left + right - i], right - i)) + 1

            while i + temp < len(check_str) and i - temp >= 0 and check_str[i - temp] == check_str[i + temp]:
                temp += 1
            temp -= 1
            res[i] = temp
            if i + temp > right:
                left = i - temp
                right = i + temp
        return sum(res)

    def odd_count():
        left = 0
        right = -1
        res = [0] * len(check_str)
        for i in range(len(check_str)):
            temp = (0 if i > right else min(res[left + right - i + 1], right - i + 1)) + 1

            while i + temp - 1 < len(check_str) and i - temp >= 0 and check_str[i - temp] == check_str[i + temp - 1]:
                temp += 1
            temp -= 1
            res[i] = temp
            if i + temp - 1 > right:
                left = i - temp
                right = i + temp - 1
        return sum(res)

    return even_count() + odd_count()


def get_unique_permutations(check_str):
    """  """

    def get_unique_permutations_helper(letter):
        """  """
        if letter == 'a':
            return ['a']
        p_list = get_unique_permutations_helper(chr(ord(letter) - 1))
        res = []
        for p in p_list:
            for idx in range(0, len(p) + 1):
                res.append(p[:idx] + letter + p[idx:])
        return res

    return get_unique_permutations_helper(check_str)


def compress(check_str):
    """  """
    compress_len = 0
    cur_count = 0
    for idx in range(1, len(check_str) + 1):
        cur_count += 1
        if idx >= len(check_str) or check_str[idx - 1] != check_str[idx]:
            compress_len += len(str(cur_count)) + len(check_str[idx - 1])
            cur_count = 0
    if compress_len >= len(check_str):
        return check_str

    sb = []
    cur_count = 0
    for idx in range(1, len(check_str) + 1):
        cur_count += 1
        if idx >= len(check_str) or check_str[idx - 1] != check_str[idx]:
            sb.append(check_str[idx - 1])
            sb.append(str(cur_count))
            cur_count = 0
    return ''.join(sb)


def search_substring_kmp(s, sub_s):
    """ """
    if not sub_s or not s:
        return 0

    m = len(sub_s)

    def prefix(check_str):
        res = [0] * len(check_str)
        for _i in range(1, len(check_str)):
            j = res[_i - 1]
            while j > 0 and check_str[j] != check_str[_i]:
                j = res[j - 1]
            if check_str[j] == check_str[_i]:
                j += 1
            res[_i] = j
        return res

    p = prefix(sub_s + '#' + s)
    return p.count(m)


def search_substring_rabin_karp(s, sub_string):
    """  """
    if not s or not sub_string:
        return 0

    count = 0
    m = len(sub_string)

    h_sub_s = hash(sub_string)

    for i in range(len(s) - m + 1):
        h_s = hash(s[i:i + m])
        if h_s == h_sub_s:
            count += 1
    return count


def search_substring_naive(s, sub_s):
    """  """
    if not s or not sub_s:
        return 0

    m = len(sub_s)
    count = 0
    for i in range(len(s) - m + 1):
        if s[i:i + m] == sub_s:
            count += 1
    return count


def search_substring_boyer_moore(s, sub_string):
    """  """
    if not s or not sub_string:
        return 0

    count = 0
    m = len(sub_string)

    def pre_bm_bc():
        res = {}
        for char_code in range(ord('a'), ord('z') + 1):
            res[chr(char_code)] = m

        for _i in range(m - 1):
            res[sub_string[_i]] = m - 1 - _i
        return res

    def pre_bm_gc():
        def is_prefix(p):
            _i = 0
            for _j in range(p, m):
                if s[_j] != s[_i]:
                    return False
                _i += 1
            return True

        def suffix_len(p):
            _len = 0
            _i = p
            _j = m - 1
            while _i >= 0 and s[_i] == s[_j]:
                _len += 1
                _i -= 1
                _j -= 1
            return _len

        res = [0] * m
        last_prefix_idx = m
        for _i in reversed(range(m)):
            if is_prefix(_i + 1):
                last_prefix_idx = _i + 1
            res[m - 1 - _i] = last_prefix_idx - _i + m - 1
        for _i in range(m - 2):
            s_len = suffix_len(_i)
            res[s_len] = m - 1 - _i + s_len
        return res

    bm_gs = pre_bm_gc()
    bm_bc = pre_bm_bc()

    _i = m - 1
    while _i <= len(s) - 1:
        j = m - 1
        while sub_string[j] == s[_i]:
            if j == 0:
                count += 1
                break
            j -= 1
            _i -= 1
        _i += max(bm_gs[m - 1 - j], bm_bc[s[_i]])

    return count
