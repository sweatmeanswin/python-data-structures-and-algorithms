"""

"""

import unittest

from base_test import BaseTestInstance

from . import string as string_func


class StringTestCase(unittest.TestCase):

    @staticmethod
    def _sub_str_search_params():
        yield from (
            ('012pale789pale', 'pale', 2),
            ('papapapa', 'papa', 3),
            ('aaaaa', 'a', 5),
            ('aaaaa', 'b', 0),
            ('aa', 'aa', 1),
            ('', '', 0),
        )

    def test_001_a(self):
        """  """
        for check_str, result in (
            ('abc', True),
            (str(list(range(10))), False),
            ('abcdefghiqrstuvwxyz', True),
            ('abcdefghijktuvwxyza', False),
        ):
            self.assertEquals(
                string_func.is_chars_unique(check_str),
                result,
                check_str
            )

    def test_palindrome_permutation(self):
        for _str, _result in (
            ('Tact Coa', True),
            ('ABBA', True),
            ('ABAA', False),
        ):
            self.assertEqual(
                string_func.is_palindrome_permutation(_str),
                _result,
                _str,
            )

    def test_one_edit(self):
        for a, b, result in (
            ('pale', 'ple', True),
            ('pales', 'pale', True),
            ('pale', 'bale', True),
            ('pale', 'bae', False),
        ):
            self.assertEqual(
                string_func.is_editable_strings(a, b),
                result,
                '{} {}'.format(a, b),
            )

    def test_string_compression(self):
        for check_str, result in (
            ('aabcccccaaa', 'a2b1c5a3'),
            ('aa', 'aa'),
            ('pale', 'pale'),
            ('paaaaaaaaaale', 'p1a10l1e1')
        ):
            self.assertEqual(
                string_func.compress(check_str),
                result,
                check_str,
            )

    def test_search_substring(self):
        """  """
        for check_str, sub_str, result in self._sub_str_search_params():
            for func in (
                string_func.search_substring_rabin_karp,
                string_func.search_substring_naive,
                string_func.search_substring_kmp,
                string_func.search_substring_boyer_moore,
            ):
                self.assertEqual(
                    func(check_str, sub_str),
                    result,
                    '{} failed: [{}:{}]'.format(
                        func.__name__,
                        check_str,
                        sub_str,
                    )
                )

    def test_is_palindrom(self):
        for check_str, res in (
            ('papa', False),
            ('apapa', True),
            ('adaldhyoiqyoiqyeoqiwe', False),
        ):
            self.assertEqual(
                string_func.is_palindrome(check_str),
                res,
                check_str,
            )

    def test_palindrom_count(self):
        for check_str, res in (
            ('papa', 2),
            ('apapa', 4),
            ('paap', 2),
            ('100001', 7),
        ):
            for func in (
                string_func.get_palindrom_count_force,
                string_func.get_palindrom_count_optimize,
                string_func.manacker,
            ):
                self.assertEqual(
                    func(check_str),
                    res,
                    func.__name__ + ": " + check_str,
                )


class StringAlgTestInstance(BaseTestInstance):
    """

    """

    TEST_CASES = [StringTestCase]
