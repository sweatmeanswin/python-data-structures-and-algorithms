"""

"""


from .base import BaseSorting


class InsertSorting(BaseSorting):
    """

    """

    @classmethod
    def sort(cls, array_list):
        """  """
        arr_len = len(array_list)
        for i in range(1, arr_len):
            j = i
            while j > 0 and array_list[j - 1] > array_list[j]:
                cls.swap(array_list, j - 1, j)
                j -= 1
