"""

"""

from .base import BaseSorting


class QuickSortingCycle(BaseSorting):
    """

    """

    @classmethod
    def sort(cls, array_list):
        """  """
        if not array_list:
            return
        cls._sort(array_list, 0, len(array_list) - 1)

    @classmethod
    def _sort(cls, array_list, lo, hi):
        """
        Args:
            array_list:
            lo:
            hi:
        """
        v = array_list[(lo + hi) // 2]
        i = lo
        j = hi
        while i <= j:
            while array_list[i] < v:
                i += 1
            while array_list[j] > v:
                j -= 1
            if i <= j:
                cls.swap(array_list, i, j)
                i += 1
                j -= 1

        if lo < j:
            cls._sort(array_list, lo, j)
        if i < hi:
            cls._sort(array_list, i, hi)
