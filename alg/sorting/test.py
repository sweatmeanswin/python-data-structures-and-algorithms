"""

"""

from random import randint
from reprlib import repr
from time import perf_counter

from base_test import BaseTestInstance

from .base import BaseSorting
from . import ALL


class SortingTestInstance(BaseTestInstance):
    """

    """

    TEST_CASES = NotImplemented
    DEFAULT_ARRAY_SIZE = 1000
    DEFAULT_PRECISION = 100

    def __init__(self, sorting_classes, array_size=DEFAULT_ARRAY_SIZE, precision=DEFAULT_PRECISION, verbose=False):
        """  """
        assert bool(sorting_classes), "Empty sorting classes list"
        for c in sorting_classes:
            if not issubclass(c, BaseSorting):
                raise TypeError(str(c.__class__))
        self._classes = list(sorting_classes)
        self._array_size = array_size
        self._precision = precision
        self._verbose = verbose
        self._result = {}

    def process(self):
        """ """
        for sort_class in self._classes:
            self._result[sort_class.__name__] = self._check_sorting(sort_class)
        return self.result

    @property
    def result(self):
        """  """
        assert bool(self._result), "Empty result. Process didn't called"
        return self._result

    @classmethod
    def test(cls, show_console=False):
        """  """
        ch = cls(
            sorting_classes=ALL,
            precision=1,
            verbose=show_console,
        )
        ch.process()

    def _check_sorting(self, sort_instance):
        """  """
        result = {
            'precision': self._precision,
            'cases': [],
        }
        for al in self._get_array_list(self._array_size):
            if [el for el in al if el < 0] and not sort_instance.CAN_NEGATIVE:
                continue

            elapsed_time = []
            al_sorted = list(sorted(al))
            for _ in range(self._precision):
                al_copy = al.copy()
                pf = perf_counter()
                sort_instance.sort(al_copy)
                elapsed_time.append(perf_counter() - pf)
                assert al_copy == al_sorted, "{} failed. sizes [{}],[{}]. different numbers: {}\n{}".format(
                    sort_instance.__name__,
                    len(al_copy),
                    len(al_sorted),
                    len([True for idx in range(self._array_size) if al_copy[idx] != al_sorted[idx]]),
                    repr(al),
                )
            avg = sum(elapsed_time) / self._precision
            if self._verbose:
                print("-> algorithm: {}".format(sort_instance.__name__))
                print("-> size: {}".format(len(al)))
                print("-> data: {}".format(repr(al)))
                print("-> precision: {}".format(self._precision))
                print("-> elapsed: {0:0.6f}".format(avg))
            result['cases'].append({
                'size': len(al),
                'data': repr(al),
                'avg': avg,
                'min': min(elapsed_time),
                'max': max(elapsed_time),
            })
        return result

    @staticmethod
    def _get_array_list(n):
        yield []
        yield [n]
        yield [n, n]
        yield [n for _ in range(n)]
        yield list(range(n))
        yield list(reversed(range(n)))
        yield [randint(0, n * 2) for _ in range(n)]
        yield [randint(-n * 2, 0) for _ in range(n)]
        yield [randint(-n, n) for _ in range(n)]


if __name__ == '__main__':
    SortingTestInstance.test()
