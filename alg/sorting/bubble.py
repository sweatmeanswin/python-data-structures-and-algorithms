"""

"""

from .base import BaseSorting


class BubbleSorting(BaseSorting):
    """

    """

    @classmethod
    def sort(cls, array_list):
        """  """
        for i in range(len(array_list)):
            for j in range(1, len(array_list) - i):
                if array_list[j - 1] > array_list[j]:
                    cls.swap(array_list, j - 1, j)


class BubbleImprovedSorting(BaseSorting):
    """

    """

    @classmethod
    def sort(cls, array_list):
        """  """
        arr_len = len(array_list)
        for i in range(arr_len):
            swapped = False
            for j in range(1, arr_len - i):
                if array_list[j - 1] > array_list[j]:
                    cls.swap(array_list, j - 1, j)
                    swapped = True
            if not swapped:
                return
