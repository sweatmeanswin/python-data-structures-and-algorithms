"""

"""

from .base import BaseSorting


class RadixSorting(BaseSorting):
    """

    """

    CAN_NEGATIVE = False
    MODULUS = 10

    @classmethod
    def sort(cls, array_list):
        m = cls.MODULUS
        div = 1

        storage = [list() for _ in range(cls.MODULUS)]
        while True:
            for l in storage:
                l.clear()

            for el in array_list:
                imp_digit = (el % m) // div
                storage[imp_digit].append(el)
            m *= cls.MODULUS
            div *= cls.MODULUS

            if len(storage[0]) == len(array_list):
                return storage[0]

            array_list.clear()
            for block in storage:
                array_list.extend(block)
