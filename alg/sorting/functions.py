"""

"""


def sub_sort(array_list):
    """  """
    end_left = 0
    while end_left + 1 < len(array_list) and array_list[end_left] < array_list[end_left + 1]:
        end_left += 1
    if end_left + 1 == len(array_list):
        return 0, 0

    start_right = len(array_list) - 1
    while start_right - 1 >= 0 and array_list[start_right - 1] < array_list[start_right]:
        start_right -= 1
    if start_right < 0:
        return 0, 0

    max_idx = end_left
    min_idx = start_right
    for i in range(end_left, start_right):
        if array_list[i] < array_list[min_idx]:
            min_idx = i
        if array_list[i] > array_list[max_idx]:
            max_idx = i

    left = 0
    for i in reversed(range(0, end_left - 1)):
        if array_list[i] <= array_list[min_idx]:
            left = i + 1
            break

    right = len(array_list) - 1
    for i in range(start_right, len(array_list)):
        if array_list[i] >= array_list[max_idx]:
            right = i - 1
            break

    return left, right
