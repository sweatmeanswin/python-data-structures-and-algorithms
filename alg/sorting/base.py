"""

"""

from abc import abstractmethod


class BaseSorting:
    """

    """

    CAN_NEGATIVE = True

    @classmethod
    @abstractmethod
    def sort(cls, array_list):
        """  """
        raise NotImplementedError()

    @staticmethod
    def swap(array_list, i, j):
        """  """
        t = array_list[i]
        array_list[i] = array_list[j]
        array_list[j] = t
