"""

"""

from .library import PythonSorting
from .quick_sort import QuickSortingCycle
from .bubble import BubbleSorting, BubbleImprovedSorting
from .insert import InsertSorting
from .merge import MergeSorting
from .radix import RadixSorting

ALL = (
    RadixSorting,
    QuickSortingCycle, MergeSorting,
    BubbleSorting, BubbleImprovedSorting,
    PythonSorting, InsertSorting,
)
