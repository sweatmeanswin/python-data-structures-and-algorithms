"""

"""

from .base import BaseSorting


class MergeSorting(BaseSorting):
    """

    """

    @classmethod
    def sort(cls, array_list):
        """  """
        if not array_list:
            return

        def merge(left, right):
            res = []
            l_idx, r_idx = 0, 0
            while l_idx < len(left) and r_idx < len(right):
                if left[l_idx] <= right[r_idx]:
                    res.append(left[l_idx])
                    l_idx += 1
                else:
                    res.append(right[r_idx])
                    r_idx += 1

            res.extend(left[l_idx:])
            res.extend(right[r_idx:])
            return res

        def sort_helper(lo, hi):
            """  """
            if lo >= hi:
                return [array_list[hi]]

            mid = (lo + hi) // 2
            left = sort_helper(lo, mid)
            right = sort_helper(mid + 1, hi)
            return merge(left, right)

        array_list[:] = sort_helper(0, len(array_list) - 1)


def merge_a_to_b(a, b, a_len, b_len):
    idx_a = a_len - 1
    idx_b = b_len - 1
    idx_merged = a_len + b_len - 1

    while idx_b >= 0:
        if idx_a >= 0 and a[idx_a] > b[idx_b]:
            a[idx_merged] = a[idx_a]
            idx_a -= 1
        else:
            a[idx_merged] = b[idx_b]
            idx_b -= 1
        idx_merged -= 1
