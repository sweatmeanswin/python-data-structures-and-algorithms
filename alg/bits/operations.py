"""

"""


def get_bit(value, pos):
    return int((value & (1 << pos)) != 0)


def set_bit(value, pos):
    return value | (1 << pos)


def int_to_binary(value):
    bits = []
    while value:
        bits.append(value & 1)
        value >>= 1
    return ''.join(map(str, reversed(bits)))


def bit_count_to_equal(first_value, second_value):
    count = 0
    while first_value or second_value:
        if first_value & 1 != second_value & 1:
            count += 1
        first_value >>= 1
        second_value >>= 1
    return count


def add(a, b):
    if b == 0:
        return a
    s = a ^ b
    c = (a & b) << 1
    return add(s, c)
