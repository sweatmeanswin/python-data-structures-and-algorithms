"""

"""

import unittest

from base_test import BaseTestInstance

from . import operations as bit_operations


class BitTestCase(unittest.TestCase):
    """  """

    def test_get_bit(self):
        for value, pos in (
            (8, 3),
            (4, 2),
            (2, 1),
            (1, 0),
        ):
            self.assertTrue(bit_operations.get_bit(value, pos))
        for i in range(1, 100):
            value = (1 << i) - 1
            for pos in range(i):
                self.assertTrue(bit_operations.get_bit(value, pos))

    def test_to_binary(self):
        for value, result in (
                (7, '111'),
                (8, '1000'),
                (1023, '1111111111'),
                (1024, '10000000000'),
        ):
            self.assertEqual(
                bit_operations.int_to_binary(value),
                result,
            )

    def test_bit_count_to_equal(self):
        for f, s, result in (
            (1024, 1023, 11),
            (29, 15, 2),
            (1 << 1000, (1 << 1000) - 1, 1001)
        ):
            self.assertEqual(
                bit_operations.bit_count_to_equal(f, s),
                result,
            )

    def test_bit_add(self):
        for a, b in (
            (10, 15),
        ):
            self.assertEqual(
                bit_operations.add(a, b),
                a + b,
            )


class BitsTestInstance(BaseTestInstance):
    """

    """

    TEST_CASES = [BitTestCase]
