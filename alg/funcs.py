"""

"""


def backpack_with_repeats(weights, costs, max_weight):
    K = [0 for _ in range(max_weight + 1)]

    for w in range(1, max_weight + 1):
        max_value = 0
        for i in range(len(weights)):
            cost = K[w - weights[i]] + costs[i]
            if weights[i] <= w and cost > max_value:
                max_value = cost
        K[w] = max_value
    return K[max_weight]


def backpack(weights, costs, max_weight):
    K = [[0 for _ in range(len(weights) + 1)] for _ in range(max_weight + 1)]

    for j in range(0, len(weights)):
        for w in range(1, max_weight + 1):
            if weights[j] > w:
                K[w][j + 1] = K[w][j]
            else:
                K[w][j + 1] = max(K[w][j], K[w - weights[j]][j] + costs[j])
    return K[max_weight][len(weights)]


def edit_distance(first_word, second_word):
    distances = [[0 for _ in range(len(second_word) + 1)] for _ in range(len(first_word) + 1)]

    def diff(a, b):
        return 0 if a == b else 1

    for i in range(1, len(first_word) + 1):
        distances[i][0] = i
    for j in range(1, len(second_word) + 1):
        distances[0][j] = j

    for i in range(len(first_word)):
        for j in range(len(second_word)):
            distances[i + 1][j + 1] = min(
                distances[i][j] + diff(first_word[i], second_word[j]),
                distances[i][j + 1] + 1,
                distances[i + 1][j] + 1,
            )
    return distances[len(first_word)][len(second_word)]


def staircase_problem(n, steps):
    cache = {}

    def helper(available):
        if available < 0:
            return 0
        elif available == 0:
            return 1
        else:
            if available not in cache:
                cache[available] = sum(
                    helper(available - step)
                    for step in steps if step <= available
                )
            return cache[available]

    return helper(n)


def make_change(n, coins):
    ways_count = [0] * (n + 1)
    ways_count[0] = 1
    for coin in coins:
        for i in range(coin, len(ways_count)):
            ways_count[i] += ways_count[i - coin]
    return ways_count[n]


def left_rotation_with_half_buffer(arr, shift):
    shift %= len(arr)
    if shift == 0:
        return arr

    move_left = shift <= (len(arr) // 2)
    if move_left:
        buffer = arr[:shift]
        arr_idx = len(arr) - shift
    else:
        buffer = arr[shift:]
        arr_idx = 0

    if move_left:
        for i in range(shift, len(arr)):
            arr[i - shift] = arr[i]
    else:
        for i in reversed(range(shift)):
            arr[i + len(arr) - shift] = arr[i]

    for el in buffer:
        arr[arr_idx] = el
        arr_idx += 1
    return arr


def left_rotation_with_buffer(arr, shift):
    shift %= len(arr)
    if shift == 0:
        return arr

    buffer = [0] * len(arr)
    for idx, el in enumerate(arr):
        buffer[(len(arr) + idx - shift) % len(arr)] = el
    return buffer


def left_rotation_without_memory(arr, shift):
    shift %= len(arr)
    if shift == 0:
        return arr

    swap_count = 0
    for i in range(shift):
        if swap_count >= len(arr):
            break

        idx = i
        prev = arr[i]

        while True:
            new_idx = (len(arr) + idx - shift) % len(arr)
            t = arr[new_idx]
            arr[new_idx] = prev
            prev = t
            idx = new_idx

            swap_count += 1
            if idx == i or swap_count >= len(arr):
                break

    return arr


def right_rotation_with_half_buffer(arr, shift):
    shift %= len(arr)
    if shift == 0:
        return arr

    move_left = shift > (len(arr) // 2)
    if move_left:
        buffer = arr[:-shift]
        arr_idx = shift
    else:
        buffer = arr[-shift:]
        arr_idx = 0

    if move_left:
        for i in range(shift):
            arr[i] = arr[i + len(arr) - shift]
    else:
        for i in reversed(range(shift, len(arr))):
            arr[i] = arr[i - shift]

    for el in buffer:
        arr[arr_idx] = el
        arr_idx += 1
    return arr


def right_rotation_with_buffer(arr, shift):
    shift %= len(arr)
    if shift == 0:
        return arr

    buffer = [0] * len(arr)
    for idx, el in enumerate(arr):
        buffer[(idx + shift) % len(arr)] = el
    return buffer


def right_rotation_without_memory(arr, shift):
    shift %= len(arr)
    if shift == 0:
        return arr

    _swap_count = 0

    for i in range(shift):
        if _swap_count >= len(arr):
            break

        idx = i
        prev = arr[i]
        while True:
            n_idx = (idx + shift) % len(arr)
            t = arr[n_idx]
            arr[n_idx] = prev
            prev = t

            idx = n_idx
            _swap_count += 1

            if idx == i or _swap_count >= len(arr):
                break

    return arr


def abbreviation(a, b):
    if set(b).difference(a):
        return 'NO'

