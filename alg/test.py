"""

"""

import unittest

from base_test import BaseTestInstance

from . import funcs as alg_funcs


class AlgFuncsTestCase(unittest.TestCase):
    """

    """

    def test_edit_distance(self):
        self.assertEqual(
            alg_funcs.edit_distance('sunny', 'snowy'), 3
        )
        self.assertEqual(
            alg_funcs.edit_distance('exponential', 'polynomial'), 6
        )

    def test_back_pack(self):
        self.assertEqual(
            alg_funcs.backpack([6, 3, 4, 2], [30, 14, 16, 9], 10),
            46,
        )
        self.assertEqual(
            alg_funcs.backpack_with_repeats([6, 3, 4, 2], [30, 14, 16, 9], 10),
            48,
        )

    def test_staircase_problem(self):
        self.assertEqual(
            alg_funcs.staircase_problem(5, [1, 2, 3]),
            13,
        )

    def test_make_change(self):
        self.assertEqual(
            alg_funcs.make_change(4, list(range(1, 4))),
            4,
        )

        self.assertEqual(
            alg_funcs.make_change(12, [1, 2, 5, 10]),
            15,
        )

    def test_rotation(self):
        n = 777
        arr = list(range(1, n + 1))
        for shift in range(n + 1):
            canonic_rotation = arr[shift:] + arr[:shift]

            for func in (
                alg_funcs.left_rotation_with_buffer,
                alg_funcs.left_rotation_with_half_buffer,
                alg_funcs.left_rotation_without_memory,
            ):
                arr_copy = arr.copy()
                self.assertEqual(
                    func(arr_copy, shift),
                    canonic_rotation,
                    '{} failed with {} shift'.format(
                        func.__name__,
                        shift,
                    )
                )

            canonic_rotation = arr[-shift:] + arr[:-shift]

            for func in (
                alg_funcs.right_rotation_with_buffer,
                alg_funcs.right_rotation_with_half_buffer,
                alg_funcs.right_rotation_without_memory,
            ):
                arr_copy = arr.copy()
                self.assertEqual(
                    func(arr_copy, shift),
                    canonic_rotation,
                    '{} failed with {} shift'.format(
                        func.__name__,
                        shift,
                    )
                )

    def test_abbreviation(self):
        alg_funcs.abbreviation('a', 'A')


class AlgFuncsTestInstance(BaseTestInstance):
    """

    """

    TEST_CASES = [AlgFuncsTestCase]
