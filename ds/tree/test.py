"""

"""

import unittest
from math import log2
from random import randint

from base_test import BaseTestInstance

from .binary import BinaryTree


class TreeTestCase(unittest.TestCase):
    """

    """

    DEFAULT_ELEMENTS_ADD_COUNT = 1000

    def setUp(self):
        """  """
        self._tree = BinaryTree()

    def _add_values(self, n=DEFAULT_ELEMENTS_ADD_COUNT):
        """  """
        for idx in range(n):
            self._tree.add(randint(0, self.DEFAULT_ELEMENTS_ADD_COUNT))

    def test_00_adding(self):
        """  """
        self._add_values()

    def test_01_length(self):
        """  """
        self.assertEqual(
            self._tree.height(),
            0
        )
        add_value_count = 5
        for idx in range(1, 10):
            self._add_values(add_value_count)
            self.assertGreater(
                self._tree.height(),
                0
            )

    def test_02_balanced(self):
        """  """
        self._add_values()
        self.assertFalse(self._tree.is_full_binary())

    def test_create_min_bst(self):
        """  """
        for shift in range(2, 20):
            s = 1 << shift
            _list = list(sorted(range(1, s)))
            _tree = BinaryTree.create_min_bst(_list)
            self.assertTrue(self._tree.is_full_binary())
            self.assertTrue(self._tree.is_balanced())
            self.assertEqual(_tree.height(), int(log2(s)), s)

    # def test_02_inserting(self):
    #     self._add_values()
    #     #
    #     self._tree.insert(0, -1)
    #     self._tree.insert(0, -2)
    #     #
    #     for _ in range(10):
    #         pos = randint(0, self._tree.length() - 1)
    #         self._tree.insert(pos, pos)
    #     #
    #     with self.assertRaises(Exception):
    #         self._tree.insert(self._tree.length(), 666)
    #
    # def test_03_indexing(self):
    #     self._add_values()
    #     for _ in range(10):
    #         pos = randint(0, self._tree.length() - 1)
    #         self._tree.at(pos)
    #     self._tree.at(0)
    #     self._tree.at(self._tree.length() - 1)
    #     with self.assertRaises(Exception):
    #         self._tree.at(self._tree.length())
    #
    # def test_04_removing(self):
    #     self._add_values()
    #
    #     node = self._tree.at(4)
    #     self._tree.remove(node)
    #     node = self._tree.at(0)
    #     self._tree.remove(node)
    #     node = self._tree.at(self._tree.length() - 1)
    #     self._tree.remove(node)
    #     self._tree.remove(node)
    #
    # def test_05_pop(self):
    #     self._add_values()
    #
    #     for _ in range(10):
    #         self._tree.pop()
    #
    # def test_06_edge_values(self):
    #     with self.assertRaises(Exception):
    #         self._tree.front()
    #         self._tree.top()
    #
    #     self._add_values()
    #
    #     for _ in range(10):
    #         self._tree.front()
    #         self._tree.top()
    #         self._tree.remove(self._tree.at(0))
    #         self._tree.pop()
    #
    # def test_remove_duplicates(self):
    #     numbers = 250
    #     for v in range(numbers):
    #         for _ in range(numbers):
    #             self._tree.add(v)
    #
    #     self._tree.remove_duplicates()
    #     self.assertEqual(self._tree.length(), numbers)
    #
    # def test_remove_duplicates_brute_force(self):
    #     numbers = 5
    #     for v in range(numbers):
    #         for _ in range(numbers):
    #             self._tree.add(v)
    #
    #     self._tree.remove_duplicates_brute_force()
    #     self.assertEqual(self._tree.length(), numbers)
    #
    # def test_add(self):
    #     # Init
    #     first_list = LinkedList()
    #     second_list = LinkedList()
    #     number = []
    #     # Fill
    #     for _ in range(self.DEFAULT_ELEMENTS_ADD_COUNT):
    #         el = randint(0, 9)
    #         number.append(el)
    #         first_list.add(el)
    #         second_list.add(el)
    #     number = int(''.join(map(str, reversed(number))))
    #     self.assertEqual(number, int(first_list))
    #     self.assertEqual(number, int(second_list))
    #     add_res = first_list + second_list
    #     double_number = number * 2
    #     self.assertEqual(
    #         double_number,
    #         int(add_res),
    #     )


class TreeTestInstance(BaseTestInstance):
    """

    """

    TEST_CASES = [TreeTestCase]
