"""

"""


class TreeNode:
    """

    """

    def __init__(self, value, left=None, right=None):
        self.value = value
        self.left = left
        self.right = right

    def __repr__(self):
        cn = self.__class__.__name__
        child = cn + "[...]"
        return '{class_name}{{v={value}, left={left}, next={right}}}'.format(
            class_name=cn,
            value=self.value,
            left=child if self.left else 'None',
            right=child if self.right else 'None',
        )
