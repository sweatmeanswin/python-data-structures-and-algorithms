"""

"""

from .node import TreeNode


class BinaryTree:
    """

    """

    def __init__(self):
        """  """
        self.head = None

    def add(self, value):
        """  """
        new_node = TreeNode(value, None, None)

        if not self.head:
            self.head = new_node
            return

        node = self.head
        while node:
            if value <= node.value:
                if node.left:
                    node = node.left
                else:
                    node.left = new_node
                    return
            else:
                if node.right:
                    node = node.right
                else:
                    node.right = new_node
                    return

    def is_bst(self):
        def is_bst_helper(node, mi, ma):
            if ma and node.value > ma or mi and node.value <= mi:
                return False

            return is_bst_helper(node.left, mi, node.value) and is_bst_helper(node.right, node.value, ma)

        return is_bst_helper(self.head, None, None)

    def is_balanced(self):
        def is_balanced_helper(node):
            if not node:
                return -1
            left = self._node_height(node.left, 0)
            if left == -2:
                return -2
            right = self._node_height(node.right, 0)
            if right == -2:
                return -2

            diff = abs(left - right)
            if diff > 1:
                return False
            else:
                return max(left, right) + 1

        return is_balanced_helper(self.head) != -2

    def is_full_binary(self):
        """  """
        def is_full_binary_helper(node):
            if not node:
                return True
            children = (1 if node.left else 0) + (1 if node.right else 0)
            if children == 2:
                return is_full_binary_helper(node.left) and is_full_binary_helper(node.right)
            return children == 0

        return is_full_binary_helper(self.head)

    def height(self):
        """  """
        return self._node_height(self.head)

    @classmethod
    def create_min_bst(cls, array_list):
        def create_min_bst_helper(start, end):
            if start > end:
                return None
            mid = (start + end) // 2
            new_node = TreeNode(array_list[mid])
            new_node.left = create_min_bst_helper(start, mid - 1)
            new_node.right = create_min_bst_helper(mid + 1, end)
            return new_node

        new_tree = BinaryTree()
        new_tree.head = create_min_bst_helper(0, len(array_list) - 1)
        return new_tree

    @classmethod
    def _node_height(cls, node, height=1):
        """  """
        if not node:
            return height - 1
        return max(
            cls._node_height(node.left, height + 1),
            cls._node_height(node.right, height + 1)
        )

    def print_pre_order(self):
        """  """
        def print_helper(node, offset=0):
            if node:
                print('{offset}{value}'.format(
                    offset=offset * '\t',
                    value=node.value,
                ))
                print_helper(node.left, offset + 1)
                print_helper(node.right, offset + 1)

        print_helper(self.head)

    def __repr__(self):
        """  """
        return '{class_name}{{{head}}}'.format(
            class_name=self.__class__.__name__,
            head=repr(self.head) if self.head else 'Empty tree'
        )
