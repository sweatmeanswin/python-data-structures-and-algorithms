"""

"""


class TrieNode:
    def __init__(self, value):
        """  """
        self.children = []
        self.value = value

    def connect(self, node):
        """  """
        if node not in self.children:
            self.children.append(node)

    def __bool__(self):
        """  """
        return bool(self.value)

    def __str__(self):
        """  """
        return self.value or '*'

    def __repr__(self):
        """  """
        return '{class_name}[{children_count}]{{{value}}}'.format(
            class_name=self.__class__.__name__,
            children_count=len(self.children),
            value=self.value,
        )


class Trie:
    """

    """

    def __init__(self):
        """  """
        self._head = TrieNode(None)

    def add_word(self, word):
        """  """
        node = self._head

        for char in word.lower():
            for child in node.children:
                if child.value == char:
                    node = child
                    break
            else:
                new_node = TrieNode(char)
                node.connect(new_node)
                node = new_node
        node.connect(TrieNode(None))

    def check_prefix(self, prefix):
        """  """
        return bool(self._find_node(prefix))

    def find_count(self, prefix):
        """  """
        node = self._find_node(prefix)
        return self._get_child_count(node) if node is not None else 0

    def _find_node(self, prefix):
        """  """
        node = self._head
        for char in prefix.lower():
            for child in node.children:
                if child and child.value == char:
                    node = child
                    break
            else:
                return None
        return node

    def __len__(self):
        """  """
        return self._get_child_count(self._head)

    @classmethod
    def _get_child_count(cls, node):
        """  """
        count = 0
        for child in node.children:
            if child:
                if child.children:
                    count += cls._get_child_count(child)
            else:
                count += 1
        return count

    def show_all_words(self):
        """  """
        def print_helper(prefix, nodes):
            if not nodes:
                print(prefix)
                return

            for child in nodes:
                print_helper(prefix + str(child), child.children)

        print_helper('', self._head.children)

    def __repr__(self):
        """  """
        return '{class_name}{{{head}}}'.format(
            class_name=self.__class__.__name__,
            head=repr(self._head) if self._head else 'Empty trie'
        )
