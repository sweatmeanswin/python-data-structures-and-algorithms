"""

"""

import os
import re
import unittest
from random import choices

from base_test import BaseTestInstance

from .trie import Trie


class TrieTestCase(unittest.TestCase):
    """

    """

    @classmethod
    def setUpClass(cls):
        cls._words = []
        _file_path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "text.txt",
        )
        with open(_file_path, mode="r", encoding="utf-8") as f:
            cls._words = list(set(re.findall('\w+', f.read())))

    def setUp(self):
        self._trie = Trie()

    def _add_values(self):
        for word in self._words:
            self._trie.add_word(word)

    def test_000_add(self):
        self._add_values()

    def test_001_true_prefixes(self):
        self._add_values()
        for word in choices(self._words, k=len(self._words) >> 1):
            for idx in range(1, len(word) + 1):
                self.assertTrue(
                    self._trie.check_prefix(word[:idx]),
                    word[:idx],
                )

    def test_len(self):
        self._add_values()
        _len = len(self._trie)
        self.assertEqual(
            _len,
            len(self._words),
            '{} [Trie] != {} [Text]'.format(_len, len(self._words)),
        )

    def test_prefix_count(self):
        self._trie.add_word("hack")
        self._trie.add_word("hackerrank")
        self.assertEqual(
            self._trie.find_count("hac"),
            2,
        )
        self.assertEqual(
            self._trie.find_count("hak"),
            0,
        )


class TrieTestInstance(BaseTestInstance):
    """

    """

    TEST_CASES = [TrieTestCase]
