"""

"""


class HashTable:
    """

    """

    def __init__(self):
        self._containers_count = 10000
        self._containers = [[] for _ in range(self._containers_count)]

    def add(self, key, value):
        _hash, _cid = self._get_key_info(key)
        self._containers[_cid].append((_hash, value))

    def _get_key_info(self, key):
        h = hash(key)
        return h, h % self._containers_count

    def get(self, key):
        _hash, _cid = self._get_key_info(key)
        for k, v in self._containers[_cid]:
            if _hash == k:
                return v
        return None
