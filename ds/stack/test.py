"""

"""

import unittest
from random import randint

from base_test import BaseTestInstance

from .stack import Stack, EmptyStackError


class StackTestCase(unittest.TestCase):
    """

    """

    DEFAULT_ADD_COUNT = 1000

    def setUp(self):
        self._stack = Stack()

    def _add_values(self, n=DEFAULT_ADD_COUNT):
        for _ in range(n):
            v = randint(0, self.DEFAULT_ADD_COUNT << 2)
            self._stack.push(v)
            self.assertEqual(self._stack.top(), v)

    def test_add(self):
        self._add_values()

    def test_exceptions(self):
        with self.assertRaises(EmptyStackError):
            self._stack.pop()
            self._stack.push(0)
            for _ in range(2):
                self._stack.pop()

    def test_minimal(self):
        for _ in range(100):
            self._add_values(1)
            m = self._stack.minimal
            new_m = -100000
            self._stack.push(new_m)
            self.assertEqual(self._stack.minimal, new_m)
            self._stack.pop()
            self.assertEqual(self._stack.minimal, m)
            self._stack.clear()


class StackTestInstance(BaseTestInstance):
    """

    """

    TEST_CASES = [StackTestCase]
