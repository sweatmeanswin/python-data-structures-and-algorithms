"""
Module contains Stack class
"""


class EmptyStackError(Exception):
    """ Raise when pop from empty stack """


class Stack:
    """
    Stack implementation
    """

    MAX_ELEMENTS_IN_STR_REPR = 10

    def __init__(self, capacity=None):
        """
        Args:
            capacity (int|None):
        """
        self._index = 0
        self._capacity = capacity or 1
        self._storage = [None] * self._capacity
        self._min_el = None

    def push(self, value):
        """
        Args:
            value (any): some object
        """
        if self._index + 1 > self._capacity:
            self._storage.extend([None] * self._capacity)
            self._capacity *= 2
        self._storage[self._index] = value
        if not self._min_el or value < self._min_el:
            self._min_el = value
        self._index += 1

    def top(self):
        if self._index == 0:
            raise EmptyStackError()
        return self._storage[self._index - 1]

    def pop(self):
        """
        Returns:
            any: stored object

        Raises:
            EmptyStackError: pop from empty stack
        """
        if self._index == 0:
            raise EmptyStackError()
        value = self._storage[self._index - 1]
        self._index -= 1
        if self._index == 0:
            self._min_el = None
        elif value == self._min_el:
            if self._index == 1:
                self._min_el = self._storage[self._index - 1]
            else:
                self._min_el = min(*self._storage[:self._index])

        return value

    @property
    def minimal(self):
        """  """
        if self._index == 0:
            raise EmptyStackError()
        return self._min_el

    @property
    def size(self):
        """
        Returns:
            int: current stack size
        """
        return self._index

    def clear(self):
        """ Reset index """
        self._index = 0
        self._min_el = None

    def shrink_to_fit(self):
        """ Remove all unnecessary data """
        self._capacity = (self._index or 1) * 2
        self._storage = self._storage[:self._capacity]

    def __str__(self):
        """
        Returns:
            str: some data
        """
        _from = max(0, self._index - self.MAX_ELEMENTS_IN_STR_REPR)
        data = str(list(reversed(
            self._storage[_from:self._index]
        )))
        if self._index > self.MAX_ELEMENTS_IN_STR_REPR:
            data = data[:-1]
            data += ', ...}'
        return "Stack[{size}/{capacity}] {{{data}}}".format(
            size=self.size,
            capacity=self._capacity,
            data=data,
        )
