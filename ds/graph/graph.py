"""

"""


class GraphNode:
    """

    """

    def __init__(self, value, index):
        """  """
        self.children = []
        self.value = value
        self.index = index

    def connect(self, node):
        """  """
        if node not in self.children:
            self.children.append(node)

    def __repr__(self):
        """  """
        return '{class_name}[{index}]{{{children_count}}}'.format(
            class_name=self.__class__.__name__,
            index=self.index,
            children_count=len(self.children),
            value=self.value,
        )


class Graph:
    """

    """

    def __init__(self):
        """  """
        self.nodes = []

    def add(self, node):
        """  """
        if node not in self.nodes:
            self.nodes.append(node)

    def breadth_first_search(self, node):
        """  """
        _visited = [False] * len(self)
        _check_nodes = [node]
        _result = []
        while _check_nodes:
            _node = _check_nodes.pop(0)
            _result.append(_node)
            _visited[_node.index] = True
            for _child_node in _node.children:
                if not _visited[_child_node.index]:
                    _check_nodes.append(_child_node)
        return _result

    def depth_first_search(self, node):
        """  """
        _visited = [False] * len(self)
        _result = []

        def dfs_helper(check_node):
            """  """
            _visited[check_node.index] = True
            _result.append(check_node)
            for _child_node in check_node.children:
                if not _visited[_child_node.index]:
                    dfs_helper(_child_node)

        dfs_helper(node)
        return _result

    def __len__(self):
        return len(self.nodes)

    def __repr__(self):
        """  """
        return "{class_name}[{children_count}]".format(
            class_name=self.__class__.__name__,
            children_count=len(self.nodes),
        )

    @classmethod
    def fromlist(cls, array_list):
        g = cls()
        for idx in range(len(array_list)):
            new_node = GraphNode(None, idx)
            g.add(new_node)

        for idx, adjacency_list in enumerate(array_list):
            for adj in adjacency_list:
                g.nodes[idx].connect(g.nodes[adj])
        return g
