"""

"""

import unittest
from random import randint, choices

from base_test import BaseTestInstance

from .graph import Graph, GraphNode


class GraphTestCase(unittest.TestCase):

    def setUp(self):
        self._graph = Graph()

    def _add_values(self):
        for idx in range(10):
            new_node = GraphNode(randint(0, 100), len(self._graph))
            self._graph.add(new_node)
            for node in choices(
                self._graph.nodes,
                k=min(len(self._graph.nodes), 2),
            ):
                if new_node is not node:
                    node.connect(new_node)

    def _create_from_list(self, graph_list):
        self._graph = Graph.fromlist(graph_list)

    def test_000_add(self):
        self._add_values()

    def test_create_from_list(self):
        self._create_from_list([
            [1],
            [2],
            [3],
            [4],
            [5],
            [0],
        ])
        self.assertGreater(len(self._graph), 0)

    def test_001_bfs(self):
        """  """
        self._create_from_list([
            [1],
            [2],
            [0, 3],
            [2],
            [6],
            [4],
            [5],
        ])
        bfs_result = self._graph.breadth_first_search(self._graph.nodes[0])
        self.assertEqual(
            [n.index for n in bfs_result],
            [0, 1, 2, 3],
        )
        bfs_result = self._graph.breadth_first_search(self._graph.nodes[4])
        self.assertEqual(
            [n.index for n in bfs_result],
            [4, 6, 5],
        )

    def test_002_dfs(self):
        """  """
        self._create_from_list([
            [1, 4, 5],
            [3, 4],
            [1],
            [2, 4],
            [],
            [],
        ])
        dfs_result = self._graph.depth_first_search(self._graph.nodes[0])
        self.assertEqual(
            [n.index for n in dfs_result],
            [0, 1, 3, 2, 4, 5],
        )


class GraphTestInstance(BaseTestInstance):
    """

    """

    TEST_CASES = [GraphTestCase]
