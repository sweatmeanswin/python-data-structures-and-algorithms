"""

"""

import unittest

from random import randint

from base_test import BaseTestInstance

from .linked_list import LinkedList


class LinkedListTestCase(unittest.TestCase):
    """

    """

    DEFAULT_ELEMENTS_ADD_COUNT = 1000

    def setUp(self):
        """  """
        self._list = LinkedList()

    def _add_values(self, n=DEFAULT_ELEMENTS_ADD_COUNT):
        """  """
        for idx in range(n):
            self._list.add(randint(0, 1 << 10))

    def test_00_adding(self):
        """  """
        self._add_values(100)

    def test_01_length(self):
        """  """
        for _ in range(10):
            self._list.length()
            self._add_values()

    def test_02_inserting(self):
        """  """
        self._add_values()
        #
        self._list.insert(0, -1)
        self._list.insert(0, -2)
        #
        for _ in range(10):
            pos = randint(0, self._list.length() - 1)
            self._list.insert(pos, pos)
        #
        with self.assertRaises(Exception):
            self._list.insert(self._list.length(), 666)

    def test_03_indexing(self):
        """  """
        self._add_values()
        for _ in range(10):
            pos = randint(0, self._list.length() - 1)
            self._list.at(pos)
        self._list.at(0)
        self._list.at(self._list.length() - 1)
        with self.assertRaises(Exception):
            self._list.at(self._list.length())

    def test_04_removing(self):
        """  """
        self._add_values()

        node = self._list.at(4)
        self._list.remove(node)
        node = self._list.at(0)
        self._list.remove(node)
        node = self._list.at(self._list.length() - 1)
        self._list.remove(node)
        self._list.remove(node)

    def test_05_pop(self):
        """  """
        self._add_values()

        for _ in range(10):
            self._list.pop()

    def test_06_edge_values(self):
        """  """
        with self.assertRaises(Exception):
            self._list.front()
            self._list.top()

        self._add_values()

        for _ in range(10):
            self._list.front()
            self._list.top()
            self._list.remove(self._list.at(0))
            self._list.pop()

    def test_remove_duplicates(self):
        """  """
        numbers = 250
        for v in range(numbers):
            for _ in range(numbers):
                self._list.add(v)

        self._list.remove_duplicates()
        self.assertEqual(self._list.length(), numbers)

    def test_remove_duplicates_brute_force(self):
        """  """
        numbers = 5
        for v in range(numbers):
            for _ in range(numbers):
                self._list.add(v)

        self._list.remove_duplicates_brute_force()
        self.assertEqual(self._list.length(), numbers)

    def test_add(self):
        """  """
        # Init
        first_list = LinkedList()
        second_list = LinkedList()
        number = []
        # Fill
        for _ in range(self.DEFAULT_ELEMENTS_ADD_COUNT):
            el = randint(0, 9)
            number.append(el)
            first_list.add(el)
            second_list.add(el)
        number = int(''.join(map(str, reversed(number))))
        self.assertEqual(number, int(first_list))
        self.assertEqual(number, int(second_list))
        add_res = first_list + second_list
        double_number = number * 2
        self.assertEqual(
            double_number,
            int(add_res),
        )

    def test_find_kth(self):
        """  """
        bound = 100
        for func in (
            LinkedList.find_kth,
            LinkedList.find_kth_iteratively,
        ):
            _list = LinkedList()
            for idx in range(1, bound):
                _list.add(idx ** 2)

            for idx in range(1, bound):
                nd = func(_list, idx - 1)
                self.assertIsNotNone(nd, idx)
                self.assertEqual(
                    nd.value,
                    (bound - idx) ** 2,
                    idx,
                )
            for idx in (bound << 1, bound << 2, bound << 1000):
                self.assertIsNone(func(_list, idx))

            self.assertEqual(func(_list, 0).value, _list.top())
            self.assertEqual(func(_list, _list.length() - 1).value, _list.front())

    def test_loops(self):
        self._add_values()
        funcs = [LinkedList.has_loop_runners, LinkedList.has_loop_with_set]
        for f in funcs:
            self.assertIsNone(f(self._list), f.__name__)
        self._list.push_back_node(self._list.at(5))
        for f in funcs:
            self.assertIsNotNone(f(self._list), f.__name__)


class LinkedListTestInstance(BaseTestInstance):
    """

    """

    TEST_CASES = [LinkedListTestCase]
