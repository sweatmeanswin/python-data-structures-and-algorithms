"""

"""

import reprlib
from collections import Iterator, Iterable


class ListNode:
    """
    One node from LinkedList
    """

    def __init__(self, value, prev=None, nxt=None):
        self.value = value
        self.prev = prev
        self.nxt = nxt

    def __repr__(self):
        cn = self.__class__.__name__
        child = cn + "[...]"
        return '{class_name}{{{value}, prev={prev}, next={nxt} }}'.format(
            class_name=cn,
            value=self.value,
            prev=child if self.prev else 'None',
            nxt=child if self.nxt else 'None',
        )


class ListNodeIterator(Iterator):
    """
    List iterator
    """

    def __init__(self, node):
        """ Init with node """
        self._node = node

    def __iter__(self):
        """ Return self """
        return self

    def __next__(self):
        """ Go till the null """
        result = self._node
        if not result:
            raise StopIteration()
        self._node = self._node.nxt
        return result


class LinkedList(Iterable):
    """
    LinkedList implementation
    """

    def __init__(self):
        """  """
        self._head = None
        self._tail = None

    def add(self, value):
        """  """
        new_node = ListNode(value, prev=self._tail)
        if self._tail:
            self._tail.nxt = new_node
        self._tail = new_node
        if not self._head:
            self._head = self._tail

    def push_back_node(self, node):
        if self._tail:
            self._tail.nxt = node
            node.prev = self._tail
        self._tail = node
        if not self._head:
            self._head = self._tail

    def insert(self, index, value):
        """  """
        node = self.at(index)
        if node == self._head:
            self._head.prev = ListNode(value, prev=None, nxt=self._head)
            self._head = self._head.prev
        elif node is None:
            self.add(value)
        else:
            new_node = ListNode(value, prev=node.prev, nxt=node)
            node.prev.nxt = new_node
            node.prev = new_node

    def length(self):
        """  """
        count = 0
        node = self._head
        while node:
            node = node.nxt
            count += 1
        return count

    def find_kth(self, k):
        """  """
        def get_offset(node, index=[-1]):
            if not node:
                return None
            nd = get_offset(node.nxt, index)
            index[0] += 1
            if index[0] == k:
                return node
            return nd

        return get_offset(self._head)

    def find_kth_iteratively(self, k):
        """  """
        p1, p2 = self._head, self._head

        for idx in range(k):
            if not p1:
                return None
            p1 = p1.nxt
        while p1.nxt:
            p1 = p1.nxt
            p2 = p2.nxt
        return p2

    def has_loop_with_set(self):
        """  """
        nodes = set()
        node = self._head
        while node:
            if node in nodes:
                return node
            nodes.add(node)
            node = node.nxt
        return None

    def has_loop_runners(self):
        """  """
        slow = self._head
        fast = self._head
        while fast and fast.nxt:
            slow = slow.nxt
            fast = fast.nxt.nxt
            if slow is fast:
                return slow
        return None

    def at(self, index):
        """  """
        if index < 0:
            raise Exception("Bad index")
        idx = 0
        node = self._head
        while idx < index and node:
            idx += 1
            node = node.nxt
        if idx < index or not node:
            raise Exception("No such index")
        return node

    def remove(self, node):
        """  """
        if not node.nxt and not node.prev:
            return

        if node == self._head:
            if self._head.nxt:
                self._head.prev = None
                self._head = self._head.nxt
        elif node == self._tail:
            self._tail.prev.nxt = None
            self._tail = self._tail.prev
        else:
            node.prev.nxt = node.nxt
            node.nxt.prev = node.prev
        node.nxt = node.prev = None

    def front(self):
        """  """
        if not self._head:
            raise Exception("Empty list")
        return self._head.value

    def top(self):
        """  """
        if not self._tail:
            raise Exception("Empty list")
        return self._tail.value

    def pop(self):
        """  """
        if not self._tail:
            raise Exception("Empty list")
        v = self._tail.value
        self.remove(self._tail)
        return v

    def remove_duplicates(self):
        """  """
        values = {}
        node = self._head
        while node:
            next_node = node.nxt
            if node.value in values:
                self.remove(node)
            else:
                values[node.value] = None
            node = next_node

    def remove_duplicates_brute_force(self):
        """  """
        node = self._head
        while node:
            temp_node = node.nxt
            while temp_node:
                next_temp_node = temp_node.nxt
                if temp_node.value == node.value:
                    self.remove(temp_node)
                temp_node = next_temp_node
            node = node.nxt

    def __repr__(self):
        """  """
        _tail = (', tail=' + str(self._tail)) \
            if self._head != self._tail else ''
        return 'LinkedList{{[{length}], head={head}{tail}}}'.format(
            length=self.length(),
            head=str(self._head),
            tail=_tail,
        )

    def __str__(self):
        """  """
        values = []
        node = self._head
        while node:
            values.append(node.value)
            node = node.nxt
        return 'LinkedList{{[{length}]{values}'.format(
            length=self.length(),
            values=', {}]'.format(reprlib.repr(values)) if values else '}',
        )

    def __iter__(self):
        """ Return Iterator """
        return ListNodeIterator(self._head)

    def __add__(self, other):
        """  """
        result_list = LinkedList()

        f_values = [el.value for el in self]
        s_values = [el.value for el in other]
        high_bit = 0
        f_idx, s_idx = 0, 0
        f_len, s_len = len(f_values), len(s_values)
        has_first = f_idx < f_len
        has_second = s_idx < s_len
        while has_first or has_second or high_bit:
            v = (
                    (f_values[f_idx] if has_first else 0)
                    + (s_values[s_idx] if has_second else 0)
                    + high_bit
            )
            result_list.add(v % 10)
            high_bit = 1 if v > 9 else 0
            f_idx += 1
            s_idx += 1
            has_first = f_idx < f_len
            has_second = s_idx < s_len

        return result_list

    def __int__(self):
        """  """
        try:
            return int(''.join(reversed([str(v.value) for v in self])))
        except TypeError:
            return 0
