"""

"""

import unittest
from operator import lt, gt

from base_test import BaseTestInstance

from .heap import Heap


class HeapTestCase(unittest.TestCase):
    """

    """

    def setUp(self):
        self._heap_min = Heap(lt, min)
        self._heap_max = Heap(gt, max)

    def _add_values(self, count, reverse=False):
        for el in (reversed(range(count)) if reverse else range(count)):
            self._heap_min.add(el)
            self._heap_max.add(el)

    def test_000_create(self):
        self._add_values(100)

    def test_000_create_reverse(self):
        self._add_values(100, reverse=True)

    def test_min(self):
        for el in range(10):
            self._heap_min.add(el)
        for el in range(10):
            self.assertEqual(
                el,
                self._heap_min.extract_top(),
            )

    def test_max(self):
        for el in range(10):
            self._heap_max.add(el)
        for el in reversed(range(10)):
            max_el = self._heap_max.extract_top()
            self.assertEqual(
                el,
                max_el,
            )


class HeapTestInstance(BaseTestInstance):
    """

    """

    TEST_CASES = [HeapTestCase]
