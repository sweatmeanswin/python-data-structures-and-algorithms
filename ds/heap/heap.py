"""

"""


class Heap:

    def __init__(self, comp_operation, top_func):
        """  """
        self._nodes = [None]
        self._count = 0
        self._capacity = 1
        self._c_op = comp_operation
        self._t_f = top_func

    @property
    def _head(self):
        """  """
        return self._nodes[0]

    @_head.setter
    def _head(self, value):
        """  """
        self._nodes[0] = value

    def add(self, value):
        """  """
        if self._head is None:
            self._head = value
        else:
            self._insert(value)
            self._heapify()
        self._count += 1

    @property
    def top(self):
        """  """
        if self._count == 0:
            raise Exception("Heap is empty")
        return self._head

    def extract_top(self):
        """  """
        if self._count == 0:
            raise Exception("Heap is empty")

        top_el = self._head

        self._count -= 1
        self._head = self._nodes[self._count]
        node_idx = 0

        while (node_idx << 1) <= self._count:
            left_idx = (node_idx << 1) + 1
            right_idx = (node_idx << 1) + 2
            if right_idx < self._count:
                swap_idx, _ = self._t_f(
                    (left_idx, self._nodes[left_idx]),
                    (right_idx, self._nodes[right_idx]),
                    key=lambda node: node[1],  # get node value
                )
            else:
                if left_idx < self._count:
                    swap_idx, _ = left_idx, self._nodes[left_idx]
                else:
                    break

            if self._c_op(self._nodes[node_idx], self._nodes[swap_idx]):
                break

            t = self._nodes[swap_idx]
            self._nodes[swap_idx] = self._nodes[node_idx]
            self._nodes[node_idx] = t
            node_idx = swap_idx

        return top_el

    def _insert(self, value):
        """  """
        if self._count + 1 >= self._capacity:
            self._nodes.extend([None] * self._capacity)
            self._capacity *= 2
        self._nodes[self._count] = value

    def _heapify(self):
        """  """
        node_idx = self._count
        while node_idx > 0:
            parent_idx = node_idx >> 1
            if not self._c_op(self._nodes[node_idx], self._nodes[parent_idx]):
                break

            t = self._nodes[parent_idx]
            self._nodes[parent_idx] = self._nodes[node_idx]
            self._nodes[node_idx] = t
            node_idx = parent_idx

    def __repr__(self):
        """  """
        return '{class_name}{{{head}}}'.format(
            class_name=self.__class__.__name__,
            head=repr(self._head) if self._head else 'Empty heap'
        )

    def __len__(self):
        """  """
        return self._count

    @classmethod
    def fromlist(cls, array_list, comp_operation, top_func):
        """  """
        _heap = cls(comp_operation, top_func)
        _heap._head = array_list[0]
        _heap._count = len(array_list)
        _heap._nodes = array_list[1:] + [None] * _heap._count
        _heap._capacity = len(_heap._nodes) - 1
