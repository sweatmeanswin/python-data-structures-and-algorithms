"""

"""


class DisjointUnionSet:
    """

    """

    def __init__(self):
        self._parent = {}
        self._counts = {}
        self._max_size = 0

    def __repr__(self):
        return repr(self._parent) + " " + repr(self._counts)

    def find(self, x):
        self._parent.setdefault(x, x)
        if self._parent[x] != x:
            self._parent[x] = self.find(self._parent[x])
            self._counts[x] = self._counts[self._parent[x]]
        else:
            self._counts.setdefault(x, 1)
        return self._parent[x]

    def union(self, x, y):
        x_parent = self.find(x)
        y_parent = self.find(y)

        if x_parent == y_parent:
            return

        if self._counts[x_parent] + self._counts[y_parent] > self._max_size:
            self._max_size = self._counts[x_parent] + self._counts[y_parent]

        self._parent[y_parent] = x_parent
        self._counts[x_parent] += self._counts[y_parent]
        del self._counts[y_parent]

    @property
    def max_size(self):
        return self._max_size
