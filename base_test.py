"""
Contain class for testing other instances
"""

import unittest


class BaseTestInstance:
    """
    Base class for testing functionality
    """

    TEST_CASES = []

    @classmethod
    def test(cls, show_console=True):
        """ Call tests without explicit object creating with default init arguments """
        for test_case in cls.TEST_CASES:
            suite = unittest.defaultTestLoader.loadTestsFromTestCase(test_case)
            unittest.TextTestRunner().run(suite)
