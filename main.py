"""

"""

import inspect
import importlib
import importlib.util
import os
import sys
import time
from pprint import pprint

from ds.heap.test import HeapTestInstance
from ds.linked_list.test import LinkedListTestInstance
from ds.stack.test import StackTestInstance
from ds.tree.test import TreeTestInstance
from ds.trie.test import TrieTestInstance
from alg.sorting.test import SortingTestInstance
from alg.string.test import StringAlgTestInstance
from alg.search.test import SearchTestInstance

from maths import operations as math_op
from alg.sorting import merge as merge_sort_op
from alg.sorting.functions import sub_sort
from alg.test import AlgFuncsTestInstance

if __name__ == '__main__':
    for instance in (
        AlgFuncsTestInstance,
        StringAlgTestInstance,
        HeapTestInstance,
        TrieTestInstance,
        LinkedListTestInstance,
        SearchTestInstance,
        SortingTestInstance,
        StackTestInstance,
        TreeTestInstance,
    ):
        print(instance.__name__)
        instance.test()
    time.sleep(1)
    print("Работа выполнена, Милорд")
